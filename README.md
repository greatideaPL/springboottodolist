# README #

This application is a training field for Spring Boot.
It supports functionality of a Todo List where the main actors are Users who want to add some activities to do later.
This is a Spring web application configured by Gradle using Hibernate support to interact with H2 database created from the model,
without any sql scitp. Other tehnology:

*	Model, View, Controller architecture
*	Login and registration with Spring Security
*	Spring Security
*	Hibernate validation
*	Bootstrap 4
*	JSP
*	JUnit with Spring for the DAO layer 

### User stories ###

User can:
*	Log in to have access to his Todo list and completed todo
*	Edit/add/delete Todo
*	Edit password and account information  

### How do I get set up? ###

All is in the box. You have to only use gradlew bootrun in command editor or run spring app from your code editor app.


### Who do I talk to? ###

* Repo owner and admin:  Tomasz Kaczmarczyk
* Contact: tomek@greatidea.pl