package pl.greatidea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import pl.greatidea.domain.Todo;
import pl.greatidea.domain.Users;
import pl.greatidea.repository.UserRepository;

import java.util.*;
@SpringBootApplication
public class SpringBootTodoApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(SpringBootTodoApplication.class);

    @Autowired
    private UserRepository userRepository;


	public static void main(String[] args) {
		SpringApplication.run(SpringBootTodoApplication.class, args);
	}

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
        // save a couple of categories
        Users user = new Users("greatidea","Tomek","tomek@greatidea.pl","greatidea" );
        Users user28minutes = new Users("in28Minutes","Micky","tomek@greatidea.pl","dummy" );
        List<Todo> todos = new ArrayList<>();
        todos.add(new Todo(user, "Learn Spring MVC", new Date(),
                false));
        todos.add(new Todo(user, "Learn Struts", new Date(), true));
        todos.add(new Todo(user, "Learn Hibernate", new Date(),
                false));
        user.setTodos(todos);
        List<Todo> todosNextUser = new ArrayList<>();
        todosNextUser.add(new Todo(user28minutes, "Learn Spring MVC", new Date(),
                false));
        todosNextUser.add(new Todo(user28minutes, "Learn Struts", new Date(), false));
        todosNextUser.add(new Todo(user28minutes, "Learn Hibernate", new Date(),
                false));
        user28minutes.setTodos(todosNextUser);
        userRepository.save(user);
        userRepository.save(user28minutes);

        // fetch all categories
        for (Users userToprint: userRepository.findAll()) {
            logger.info(userToprint.toString());
        }
    }
}
