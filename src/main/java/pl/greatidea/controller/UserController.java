package pl.greatidea.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.greatidea.domain.Users;
import pl.greatidea.domain.dto.PasswordDto;
import pl.greatidea.security.SecurityService;
import pl.greatidea.service.PasswordValidator;
import pl.greatidea.service.UserService;
import pl.greatidea.service.UserValidator;

import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private WelcomeController controller;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new Users());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") Users userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userForm.setRole("USER");
        userService.saveUser(userForm);
        securityService.autologin(userForm.getLogin(), userForm.getPassword());
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/edit-pass", method = RequestMethod.POST)
    public String editPassword(@ModelAttribute("pass") PasswordDto pass, BindingResult bindingResult, ModelMap model ) {

        passwordValidator.validate(pass, bindingResult);

        if (bindingResult.hasErrors()) {
            return "edit-pass";
        }
        String userLogin = controller.getLoggedInUserName();
        Users user = userService.findByLogin(userLogin);
        user.setPassword(pass.getPassword());
        userService.updateUser(user);
        securityService.autologin(user.getLogin(), user.getPassword());

        return "redirect:/welcome";
    }

    @RequestMapping(value = "/edit-pass", method = RequestMethod.GET)
    public String showEditPasswordPage(ModelMap model ) {
        String userLogin = controller.getLoggedInUserName();
        Users user = userService.findByLogin(userLogin);
        PasswordDto pass = new PasswordDto();
        pass.setPassword(user.getPassword());
        model.addAttribute("pass", pass );
        return "edit-pass";
    }



    @RequestMapping(value = "/update-user", method = RequestMethod.GET)
    public String showEditUserPage(ModelMap model ) {
        String userLogin = controller.getLoggedInUserName();
        Users user = userService.findByLogin(userLogin);
        model.addAttribute("user", user );
        return "update-user";
    }

    @RequestMapping(value = "/update-user", method = RequestMethod.POST)
    public String editUserInfo( ModelMap model, @Valid Users user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            Users users = (Users) model.get("users");
            //model.clear();
            model.addAttribute("user", users );
            //TODO: Chceck unexpected model value changed
            return "update-user-error";
        }

        userService.updateUser(user);
        securityService.autologin(user.getLogin(), user.getPassword());
        model.clear();
        return "redirect:/welcome";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }
}