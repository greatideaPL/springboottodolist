package pl.greatidea.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
public class Todo {

    @Id
    @GeneratedValue
    @Column(name = "todo_id")
    private int id;

    @Size(min = 6, message = "Enter atleast 6 Characters.")
    private String desc;
    private Date targetDate;
    private boolean completed;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users user;

    public Todo() {
    }

    public Todo(Users user, String desc, Date targetDate, boolean completed) {
        this.user = user;
        this.desc = desc;
        this.targetDate = targetDate;
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public boolean getCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", user='" + user.getLogin() + '\'' +
                ", desc='" + desc + '\'' +
                ", targetDate=" + targetDate +
                ", completed=" + completed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Todo toDo = (Todo) o;

        return id == toDo.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
