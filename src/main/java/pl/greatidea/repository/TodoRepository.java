package pl.greatidea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.greatidea.domain.Todo;
import pl.greatidea.domain.Users;

import java.util.Date;
import java.util.List;

@Transactional
public interface TodoRepository extends JpaRepository<Todo, Long> {
    List<Todo> findByUser(Users user);
    List<Todo> findAllByUserAndCompleted(Users user, Boolean completed);
    Todo findById(int id);

    @Modifying
    @Query("UPDATE Todo t set t.desc = :desc, t.targetDate= :targetDate, t.completed= :completed where t.id= :id")
    void update(@Param("desc") String desc, @Param("targetDate")Date targetDate,  @Param("completed")boolean completed, @Param("id") int id);


    @Modifying
    @Query("delete from Todo u where u.id = ?1")
    void deleteTodoById(int id);

}
