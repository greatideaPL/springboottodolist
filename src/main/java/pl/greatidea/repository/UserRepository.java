package pl.greatidea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.greatidea.domain.Users;

@Transactional
public interface UserRepository extends JpaRepository<Users, Long> {
    Users findByEmail(String mail);
    Users findByLogin(String login);

    @Modifying
    @Query("UPDATE Users u set u.login = :login, u.name= :name, u.surname= :surname, u.email= :email where u.id= :id")
    void updateUser(@Param("login") String login, @Param("name")String name, @Param("surname")String surname, @Param("email")String email, @Param("id") Long id);

}
