package pl.greatidea.service;

import org.springframework.stereotype.Service;
import pl.greatidea.domain.Users;

@Service
public interface UserService {
    Users findUserByEmail(String email);
    Users findByLogin(String name);
    void saveUser(Users user);
    void updateUser(Users user);
}
