package pl.greatidea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.greatidea.domain.Users;
import pl.greatidea.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Users findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Users findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void saveUser(Users user) {
        userRepository.save(user);
    }

    @Override
    public void updateUser(Users user) {
       userRepository.updateUser(user.getLogin(), user.getName(), user.getSurname(), user.getEmail(), user.getId());
    }

    public UserServiceImpl() {
    }
}
