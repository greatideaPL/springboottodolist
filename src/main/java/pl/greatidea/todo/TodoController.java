package pl.greatidea.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomBooleanEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import pl.greatidea.domain.Users;
import pl.greatidea.service.UserService;
import pl.greatidea.domain.Todo;
import pl.greatidea.controller.WelcomeController;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@SessionAttributes("name")
public class TodoController {

    @Autowired
    private UserService userService;

    @Autowired
    private TodoService todoService;

    @Autowired
    private WelcomeController controller;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, false));
        binder.registerCustomEditor(Boolean.class, new CustomBooleanEditor(false));
        binder.registerCustomEditor(boolean.class, new CustomBooleanEditor(false));
    }


    @RequestMapping(value = "/list-todos", method = RequestMethod.GET)
    public String showTodosList(ModelMap model) {
        String user = controller.getLoggedInUserName();
        String title = "Your todo list";
        model.addAttribute("todos", todoService.retrieveTodos(user));
        model.addAttribute("title", title);
        return "list-todos";
    }

    @RequestMapping(value = "/list-completed-todos", method = RequestMethod.GET)
    public String showCompetedTodosList(ModelMap model) {
        Users user = userService.findByLogin(controller.getLoggedInUserName());
        String title = "Your completed todo list";
        model.addAttribute("todos", todoService.retrieveCompletedTodos(user));
        model.addAttribute("title", title);
        return "list-todos";
    }


    @RequestMapping(value = "/add-todo", method = RequestMethod.GET)
    public String showAddTodoPage(ModelMap model) {
        model.addAttribute("todo", new Todo());
        List booleanList = new ArrayList<Boolean>();
        booleanList.add(Boolean.FALSE);
        booleanList.add(Boolean.TRUE);
        model.addAttribute("isDoneList", booleanList);
        return "todo";
    }

    @RequestMapping(value = "/add-todo", method = RequestMethod.POST)
    public String addTodo(ModelMap model, @Valid Todo todo, BindingResult result) {

        if (result.hasErrors())
            return "todo";

        todoService.addTodo(controller.getLoggedInUserName(), todo.getDesc(),
                todo.getTargetDate(), false);
        model.clear();// to prevent request parameter "name" to be passed
        return "redirect:/list-todos";
    }


    @RequestMapping(value = "/update-todo", method = RequestMethod.GET)
    public String showUpdateTodoPage(ModelMap model, @RequestParam int id) {
        model.addAttribute("todo", todoService.retrieveTodo(id));
        List booleanList = new ArrayList<Boolean>();
        booleanList.add(Boolean.FALSE);
        booleanList.add(Boolean.TRUE);
        model.addAttribute("isDoneList", booleanList);
        return "todo";
    }

    @RequestMapping(value = "/update-todo", method = RequestMethod.POST)
    public String updateTodo(ModelMap model, @Valid Todo todo,
                             BindingResult result) {
        if (result.hasErrors())
            return "todo";

        todo.setUser(userService.findByLogin(controller.getLoggedInUserName()));
        todoService.updateTodo(todo);

        model.clear();// to prevent request parameter "name" to be passed
        return "redirect:/list-todos";
    }

    @RequestMapping(value = "/delete-todo", method = RequestMethod.GET)
    public String deleteTodo(@RequestParam int id) {
        todoService.deleteTodo(id);

        return "redirect:/list-todos";
    }

}