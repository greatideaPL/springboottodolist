package pl.greatidea.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.greatidea.domain.Todo;
import pl.greatidea.domain.Users;
import pl.greatidea.repository.TodoRepository;
import pl.greatidea.repository.UserRepository;

import java.util.Date;
import java.util.List;

@Service
public class TodoService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TodoRepository todoRepository;

    public List<Todo> retrieveTodos(String user) {
        return todoRepository.findByUser(userRepository.findByLogin(user));
    }
    public List<Todo> retrieveCompletedTodos(Users user) {
        return todoRepository.findAllByUserAndCompleted(user, Boolean.TRUE);
    }


    public Todo retrieveTodo(int id) {
        return todoRepository.findById(id);
    }

    public void updateTodo(Todo todo) {
        todoRepository.update(todo.getDesc(), todo.getTargetDate(), todo.getCompleted(), todo.getId());
    }

    public void addTodo(String name, String desc, Date targetDate,
                        boolean isDone) {
        Users user = userRepository.findByLogin(name);
        Todo todo = new Todo(user, desc, targetDate, isDone);
        todoRepository.save(todo);
    }

    public void deleteTodo(int id) {
        todoRepository.deleteTodoById(id);
    }
}