<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1>Edit your password</h1>
    </div>
</div>
<div class="container">
    <form:form method="POST" modelAttribute="pass" class="form-signin">
        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Password"
                            autofocus="true"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="confirmPassword">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="confirmPassword" class="form-control" placeholder="Confirm password"></form:input>
                <form:errors path="confirmPassword"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form:form>

</div>
<!-- /container -->

<%@ include file="common/footer.jspf"%>