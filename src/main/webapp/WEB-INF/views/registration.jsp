<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1>Create your account</h1>
    </div>
</div>
<div class="container">
    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <spring:bind path="login">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="login" class="form-control" placeholder="Login"
                            autofocus="true"></form:input>
                <form:errors path="login"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Password"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form:form>

</div>
<!-- /container -->

<%@ include file="common/footer.jspf"%>