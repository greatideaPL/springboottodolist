<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1>Todo</h1>
    </div>
</div>
<div class="container">
    <form:form method="POST" commandName="todo">
        <form:hidden path="id" />
        <fieldset class="form-group">
            <form:label path="desc">Description</form:label>
            <form:input path="desc" type="text" class="form-control"
                        required="required" />
            <form:errors path="desc" cssClass="text-warning" />
        </fieldset>
        <fieldset class="form-group">
            <form:label path="targetDate">Target Date</form:label>
            <form:input path="targetDate" type="text" class="form-control"
                        required="required" />
            <form:errors path="targetDate" cssClass="text-warning" />
        </fieldset>
        <fieldset class="form-group">
            <form:label path="completed">Completed status</form:label>
            <form:select path="completed" cssClass="custom-select">
                <form:options items="${isDoneList}"/>
            </form:select>

        </fieldset>
        <button type="submit" class="btn btn-success">Submit</button>
    </form:form>
</div>

<%@ include file="common/footer.jspf"%>
<script>
    $(document).ready(function(){
        $('#targetDate').datepicker({
            format : 'dd/mm/yyyy'
        })
    });
</script>

