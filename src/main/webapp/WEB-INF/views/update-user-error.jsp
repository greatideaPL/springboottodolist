<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1>Edit your account</h1>
    </div>
</div>
<div class="container">
    <form:form method="POST" commandName="users">
        <form:hidden path="id" />

                <fieldset class="form-group">
                    <form:label path="login">Login</form:label>
                    <form:input path="login" type="text" class="form-control" required="required"/>
                    <form:errors path="login" cssClass="text-warning" />
                </fieldset>


                <fieldset class="form-group">
                    <form:label path="name">Name</form:label>
                    <form:input path="name" type="text" class="form-control"/>
                    <form:errors path="name" cssClass="text-warning" />
                </fieldset>


                <fieldset class="form-group">
                    <form:label path="surname">Surname</form:label>
                    <form:input path="surname" type="text" class="form-control"/>
                    <form:errors path="surname" cssClass="text-warning" />
                </fieldset>


                <fieldset class="form-group">
                    <form:label path="email">E-mail</form:label>
                    <form:input path="email" type="text" class="form-control" required="required" />
                    <form:errors path="email" cssClass="text-warning" />
                </fieldset>

        <input type="submit" class="btn btn-success" value="Submit"/>
    </form:form>
    <div>
        <a type="button" class="btn btn-success" href="/edit-pass">Edit Password</a>
    </div>
</div>


<%@ include file="common/footer.jspf"%>
