<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>
<div class="jumbotron jumbotron-fluid">
<div class="container">

    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
            <h1>Welcome ${pageContext.request.userPrincipal.name}</h1>
            <p class="lead">
                Its my first spring boot application. Enjoy.
                Fell free to contact me and comment.
            </p>
    </c:if>

</div>
</div>
<!-- /container -->

<%@ include file="common/footer.jspf"%>