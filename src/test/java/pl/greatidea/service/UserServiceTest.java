package pl.greatidea.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.greatidea.domain.Users;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void saveUser(){
        userService.saveUser(new Users("olunia","Ola", "ola@greatidea.pl", "password" ));
        Users user = userService.findUserByEmail("ola@greatidea.pl");
        assertThat(user.getLogin()).isEqualTo("olunia");
        assertThat(user.getName()).isEqualTo("Ola");

    }

}
