package pl.greatidea.todo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import pl.greatidea.domain.Todo;
import pl.greatidea.domain.Users;
import pl.greatidea.repository.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest

public class TodoServiceTest {


    @Autowired
    TodoService todoService;

    @Autowired
    UserRepository userRepository;

    public static Users user;

    @Before
    public void initTest(){
        user = new Users("greatideaPl","Tomek","tomek@greatidea.pl","greatidea" );
        List<Todo> todos = new ArrayList<>();
        todos.add(new Todo(user, "Learn Spring MVC", new Date(),
                false));
        todos.add(new Todo(user, "Learn Struts", new Date(), true));
        todos.add(new Todo(user, "Learn Hibernate", new Date(),
                false));
        user.setTodos(todos);
        userRepository.save(user);
    }

    //@Ignore
    @Test
    public void retrieveCompletedTodos() throws Exception {

        List<Todo> completedTodos = todoService.retrieveCompletedTodos(user);
        assertThat(completedTodos.size()).isEqualTo(1);
    }


}
